Wallabag is **not** integrated with the Cloudron user management.

The app comes with a pre-setup admin account with the following credentials:

`username: wallabag`

`password: wallabag`

**Please change the admin password on first login**
