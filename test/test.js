#!/usr/bin/env node

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = webdriver.By,
    Keys = webdriver.Key,
    until = webdriver.until;

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

describe('Application life cycle test', function () {
    this.timeout(0);

    var chrome = require('selenium-webdriver/chrome');
    var server, browser = new chrome.Driver();

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    var LOCATION = 'test';
    var TEST_TIMEOUT = 30000;
    var USERNAME = 'wallabag';
    var PASSWORD = 'wallabag';
    var TEST_URL = 'https://cloudron.io';
    var app;

    function waitForElement(elem, callback) {
         browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT).then(function () {
                callback();
            });
        });
    }

    function login(callback) {
        // browser.manage().window().setSize(1280,768);
        browser.manage().deleteAllCookies();
        browser.get('https://' + app.fqdn);

        waitForElement(by.id('username'), function () {
            browser.findElement(by.id('username')).sendKeys(USERNAME);
            browser.findElement(by.id('password')).sendKeys(PASSWORD);
            browser.findElement(by.xpath('//form[@name="loginform"]')).submit();

            waitForElement(by.xpath('//a[@href="/logout"]'), callback);
        });
    }

    function logout(callback) {
        browser.manage().window().setSize(1280,768);
        browser.get('https://' + app.fqdn);

        waitForElement(by.xpath('//a[@href="/logout"]'), function () {
            browser.findElement(by.xpath('//a[@href="/logout"]')).click();

            waitForElement(by.id('username'), callback);
        });
    }

    function urlExists(done) {
        browser.get('https://' + app.fqdn + '/view/1');

        // search album and enter it
        waitForElement(by.xpath('//h2[text()="Instantly install Apps"]'), done);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    }

    function addLink(done) {
        browser.get('https://' + app.fqdn);

        waitForElement(by.xpath('//a[@href="/new"]'), function () {
            browser.findElement(by.xpath('//a[@href="/new"]')).click();

            waitForElement(by.id('entry_url'), function () {
                browser.findElement(by.id('entry_url')).sendKeys(TEST_URL);
                browser.findElement(by.id('entry_url')).sendKeys(webdriver.Key.ENTER);

                waitForElement(by.xpath('//a[@href="/view/1"]'), done);
            });
        });
    }

    function registrationDisabled(done) {
        superagent.get('https://' + app.fqdn + '/register').redirects(0).end(function (error, result) {
            if (result.statusCode !== 301) return done(new Error('register is not redirecting ' + result.statusCode));

            done();
        });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can add link', addLink);
    it('url exists', urlExists);
    it('can logout', logout);
    it('disabled registration', registrationDisabled);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', login);
    it('url exists', urlExists);
    it('can logout', logout);
    it('disabled registration', registrationDisabled);

    it('move to different location', function () {
        execSync('cloudron configure --wait --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('url exists', urlExists);
    it('can logout', logout);
    it('disabled registration', registrationDisabled);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --new --wait --appstore-id ' + app.manifest.id + ' --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can add link', addLink);
    it('url exists', urlExists);
    it('can logout', logout);
    it('can update', function () {
        execSync('cloudron install --wait --app ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('can login', login);
    it('url exists', urlExists);
    it('disabled registration', registrationDisabled);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
