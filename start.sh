#!/bin/bash

set -eux

mkdir -p /run/wallabag/cache /run/wallabag/logs /run/wallabag/sessions

sed -e "s/database_driver:.*/database_driver: pdo_pgsql/" \
    -e "s/database_host:.*/database_host: ${POSTGRESQL_HOST}/" \
    -e "s/database_port:.*/database_port: ${POSTGRESQL_PORT}/" \
    -e "s/database_name:.*/database_name: ${POSTGRESQL_DATABASE}/" \
    -e "s/database_user:.*/database_user: ${POSTGRESQL_USERNAME}/" \
    -e "s/database_password:.*/database_password: ${POSTGRESQL_PASSWORD}/" \
    -e "s/database_charset: .*/database_charset: utf8/" \
    -e "s,domain_name: .*,domain_name: ${APP_ORIGIN}," \
    -e "s/redis_host:.*/redis_host: ${REDIS_HOST}/" \
    -e "s/redis_port:.*/redis_port: ${REDIS_PORT}/" \
    -e "s/redis_password:.*/redis_password: ${REDIS_PASSWORD}/" \
    -e "s/mailer_host:.*/mailer_host: ${MAIL_SMTP_SERVER}:${MAIL_SMTP_PORT}/" \
    -e "s/mailer_user:.*/mailer_user: ${MAIL_SMTP_USERNAME}/" \
    -e "s/mailer_password:.*/mailer_password: ${MAIL_SMTP_PASSWORD}/" \
    -e "s/from_email:.*/from_email: ${MAIL_FROM}/" \
    -e "s/twofactor_sender:.*/twofactor_sender: ${MAIL_FROM}/" \
    -e "s/fosuser_registration:.*/fosuser_registration: false/" \
    -e "s/fosuser_confirmation:.*/fosuser_confirmation: false/" \
    /app/code/parameters.yml > /run/wallabag/parameters.yml

sed -e "s/save_path:.*/save_path: '\/run\/wallabag\/sessions'/" \
    -e "/server_version: 5.6/d" \
    /app/code/config.yml > /run/wallabag/config.yml

sed -e "s/path:.*/path: '\/run\/wallabag\/wallabag.log'/" \
    /app/code/config_prod.yml > /run/wallabag/config_prod.yml

# ensure the upload, import and images directory exists
mkdir -p /app/data/uploads/import /app/data/assets/images /app/data/data

chown -R www-data:www-data /run/wallabag /app/data

gosu="/usr/local/bin/gosu www-data:www-data"

# Install wallabag, if this is an update, the script will warn about existing user "wallabag". This is ok.
$gosu php bin/console wallabag:install --env prod --no-interaction

# run the migration scripts
$gosu php bin/console doctrine:migrations:migrate --env prod --no-interaction

# update the location
$gosu php bin/console doctrine:query:sql --env prod "UPDATE wallabag_craue_config_setting SET value='${APP_ORIGIN}' WHERE name='wallabag_url';"
$gosu php bin/console doctrine:query:sql --env prod "UPDATE wallabag_craue_config_setting SET value='1' WHERE name='import_with_redis';"

# start the import worker
$gosu php bin/console wallabag:import:redis-worker --env prod pocket -vv &
$gosu php bin/console wallabag:import:redis-worker --env prod readability -vv &
$gosu php bin/console wallabag:import:redis-worker --env prod instapaper -vv &
$gosu php bin/console wallabag:import:redis-worker --env prod wallabag_v1 -vv &
$gosu php bin/console wallabag:import:redis-worker --env prod wallabag_v2 -vv &
$gosu php bin/console wallabag:import:redis-worker --env prod firefox -vv &
$gosu php bin/console wallabag:import:redis-worker --env prod chrome -vv &

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
